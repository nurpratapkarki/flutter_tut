import 'package:first_app/src/themes/mine_app_bar.dart';
import 'package:first_app/src/themes/mine_bottom_bar.dart';
import 'package:flutter/material.dart';

class AccountScreen extends StatelessWidget {
  const AccountScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MineAppBar.appBar2(context: context, title: "Account Screen"),
      bottomNavigationBar: MineBottomBar.bottomBar(index: 3),
    );
  }
}
