import 'package:first_app/src/themes/mine_app_bar.dart';
import 'package:first_app/src/themes/mine_bottom_bar.dart';
import 'package:flutter/material.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MineAppBar.appBar2(context: context, title: "Search Screen"),
      bottomNavigationBar: MineBottomBar.bottomBar(index: 1),
    );
  }
}
