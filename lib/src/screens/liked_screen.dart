import 'package:first_app/src/themes/mine_app_bar.dart';
import 'package:first_app/src/themes/mine_bottom_bar.dart';
import 'package:flutter/material.dart';

class LikedScreen extends StatelessWidget {
  const LikedScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MineAppBar.appBar2(context: context, title: "Liked Screen"),
      bottomNavigationBar: MineBottomBar.bottomBar(index: 2),
    );
  }
}
