import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Container(
      padding: const EdgeInsets.all(20),
      child: TextField(
        decoration: InputDecoration(
            suffixIcon: Container(
              padding: const EdgeInsets.only(right: 10),
              child: const Icon(
                Icons.search,
                size: 30,
                color: Colors.grey,
              ),
            ),
            label: Container(
              margin: const EdgeInsets.only(left: 10),
              child: const Text('Search'),
            ),
            // hintText: 'Search',
            border: OutlineInputBorder(
              borderRadius: const BorderRadius.all(Radius.circular(200)),
              borderSide: BorderSide(color: Colors.green.shade500),
            )),
      ),
    )));
  }
}
