import 'package:first_app/src/constants/image_constant.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: ListTile(
        leading: CircleAvatar(backgroundImage: NetworkImage(naruto2Img)),
        title: const Text('Ravi Pangali',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
        subtitle: Text('Hello Ravi',
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
                color: Colors.grey.shade600)),
        trailing: Icon(
          Icons.delete,
          color: Colors.grey.shade700,
          size: 25,
        ),
      ),
      // child: SizedBox(
      //   height: 200,
      //   width: 200,
      //   child: CircleAvatar(
      //     backgroundImage: AssetImage(
      //       narutoImg,
      //     ),
      //   ),
      // ),
      // child: Icon(
      //   Icons.home,
      //   size: 40,
      //   color: Colors.amber.shade600,
      // ),
      // child: Stack(
      //   children: [
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.red,
      //     ),
      //     Positioned(
      //       bottom: 10,
      //       right: 10,
      //       child: Container(
      //         width: 50,
      //         height: 50,
      //         color: Colors.blue,
      //       ),
      //     )
      //   ],
      // ),
      // child: Wrap(
      //   runAlignment: WrapAlignment.center,
      //   runSpacing: 20,
      //   alignment: WrapAlignment.center,
      //   spacing: 20,
      //   children: [
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.red,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.blue,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.green,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.amber,
      //     ),
      //   ],
      // ),
      // child: ListView(
      //   scrollDirection: Axis.horizontal,
      //   children: [
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.red,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.blue,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.green,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.amber,
      //     ),
      //   ],
      // ),
      //         child: Column(
      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //   crossAxisAlignment: CrossAxisAlignment.center,
      //   children: [
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.red,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.blue,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.green,
      //     ),
      //     Container(
      //       width: 100,
      //       height: 100,
      //       color: Colors.amber,
      //     ),
      //   ],
      // ),
    ));
  }
}
