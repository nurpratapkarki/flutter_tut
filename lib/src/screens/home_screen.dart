import 'package:first_app/src/themes/mine_app_bar.dart';
import 'package:first_app/src/themes/mine_bottom_bar.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: MineAppBar.appBar(context: context, title: "Home Screen"),
        bottomNavigationBar: MineBottomBar.bottomBar(index: 0));
  }
}
