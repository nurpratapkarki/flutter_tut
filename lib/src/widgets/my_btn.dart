import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyBtn extends StatelessWidget {
  const MyBtn({
    super.key,
    required this.name,
    required this.firstColor,
    required this.secondColor,
  });

  final String name;

  final dynamic firstColor;
  final dynamic secondColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print("Arigato");
      },
      child: Container(
        height: 50,
        padding: const EdgeInsets.symmetric(horizontal: 34, vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(200)),
          gradient: LinearGradient(
              begin: Alignment.topLeft, colors: [firstColor, secondColor]),
        ),
        child: Text(name,
            style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 17)),
      ),
    );
  }
}
