import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MineAppBar {
  static appBar({required context, required title}) {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      title: Text(
        title,
        style: const TextStyle(color: Colors.pink, fontWeight: FontWeight.bold),
      ),
      leading: const Icon(
        Icons.menu,
        size: 30,
        color: Colors.pink,
      ),
      actions: [
        Container(
          padding: const EdgeInsets.only(right: 15),
          child: InkWell(
            onTap: () {
              showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(180),
                              topRight: Radius.circular(180))),
                    );
                  });
            },
            child: const FaIcon(
              FontAwesomeIcons.facebookMessenger,
              size: 25,
              color: Colors.pink,
            ),
          ),
        )
      ],
    );
  }

  static appBar2({required context, required title}) {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      foregroundColor: Colors.pink,
      title: Text(
        title,
        style: const TextStyle(color: Colors.pink, fontWeight: FontWeight.bold),
      ),
      actions: [
        Container(
          padding: const EdgeInsets.only(right: 15),
          child: InkWell(
            onTap: () {
              showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(180),
                              topRight: Radius.circular(180))),
                    );
                  });
            },
            child: const FaIcon(
              FontAwesomeIcons.facebookMessenger,
              size: 25,
              color: Colors.pink,
            ),
          ),
        )
      ],
    );
  }
}
