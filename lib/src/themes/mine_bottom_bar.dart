import 'package:first_app/src/screens/account_screen.dart';
import 'package:first_app/src/screens/home_screen.dart';
import 'package:first_app/src/screens/liked_screen.dart';
import 'package:first_app/src/screens/search_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

class MineBottomBar {
  static bottomBar({required index}) {
    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Colors.grey.shade600,
            blurRadius: 25,
            spreadRadius: -2,
            offset: const Offset(0, 15))
      ]),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      child: SalomonBottomBar(
        selectedItemColor: Colors.pink,
        currentIndex: index,
        unselectedItemColor: Colors.grey.shade600,
        items: [
          SalomonBottomBarItem(
              icon: const Icon(
                Icons.home,
                size: 25,
              ),
              title: const Text('Home')),
          SalomonBottomBarItem(
              icon: const Icon(
                Icons.search,
                size: 25,
              ),
              title: const Text('Search')),
          SalomonBottomBarItem(
              icon: const Icon(
                Icons.favorite,
                size: 25,
              ),
              title: const Text('Liked')),
          SalomonBottomBarItem(
              icon: const Icon(
                Icons.person,
                size: 25,
              ),
              title: const Text('Account')),
        ],
        onTap: (i) {
          if (i == 0) {
            Get.offAll(const HomeScreen());
          } else if (i == 1) {
            Get.to(const SearchScreen());
          } else if (i == 2) {
            Get.to(const LikedScreen());
          } else {
            Get.to(const AccountScreen());
          }
        },
      ),
    );
  }
}
